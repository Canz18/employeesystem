<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    // protected $guarded = [];
    protected $fillable = [
        'company_name', 'company_address',
    ];
    
    public function users()
    {
        return $this->hasMany(User::class);
    }
    
    public function scopeTallyGender($query)
    {
        $companies = $query->join('users', 'users.company_id', 'companies.id');
        $select = [
            'companies.id','companies.company_name','companies.company_address',
            'COUNT(IF(users.gender = "male", 1, NULL)) as total_male',
            'COUNT(IF(users.gender = "female", 1, NULL)) as total_female',
        ];
        $data = $companies
            ->selectRaw(implode(',', $select))
            ->groupBy(['companies.id','companies.company_name','companies.company_address']);
    }
}
