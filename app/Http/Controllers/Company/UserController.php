<?php

namespace App\Http\Controllers\Company;

use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request, Company $company)
    {
          return new CompanyResource($company);
        // return $company->tallyGender()->first();
         //return $company->with('users')->first();
    }
    
    public function show(Request $request, Company $company)
    {
        return $company->tallyGender();
    }
}