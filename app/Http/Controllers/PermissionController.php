<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        return Permission::all();
    }

    public function store(Request $request)
    {
        return Permission::create(['name' => $request->name]);
    }

    public function destroy(Request $request, Permission $permission)
    {
        $permission->delete();
        return [
            'message' => 'Successfully deleted role'
        ];
    }

    // public function show(Request $request, Role $role)
    // {
    //     return $role->permissions;
    // }

    public function getPermissionNotInRole(Request $request, Role $role)
    {
        return Permission::whereNotIn('id', $role->permissions->pluck('id'))->get();
    }
}
