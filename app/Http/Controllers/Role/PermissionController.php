<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index(Request $request, Role $role)
    {
        return $role->permissions;
    }

    public function destroy (Request $request, Role $role)
    {
        return $role->revokePermissionTo($request->permissions);
    }
}
