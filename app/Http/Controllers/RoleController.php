<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function store(Request $request) 
    {
        return Role::create(['name' => $request->rolename]);
    }

    public function index(Request $request)
    {
        return Role::all();
    }

    public function update(Request $request, Role $role) // /role/{role id}
    {
        return $role->givePermissionTo($request->permissions);
    }

    public function destroy(Request $request, Role $role) // /role/{role id} //delete the certain role and its permission
    {
        $role->revokePermissionTo($role->permissions);
        $role->delete();
        return [
            'message' => 'Successfully deleted role'
        ];
    }
}
