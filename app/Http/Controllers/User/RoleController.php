<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class RoleController extends Controller
{
    public function index (Request $request, User $user)
    {
        return $user->roles;
    }

    public function store(Request $request, User $user)
    {
        return $user->assignRole($request->rolename);
    }
    
    public function update(Request $request, User $user) // /role/{user id}/edit
    {
        return $user->syncRoles($request);
    }
}
