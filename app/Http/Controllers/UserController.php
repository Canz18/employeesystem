<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //
    public function index(Request $request)
    {
        return UserResource::collection(User::where('id', '!=', auth()->id())->get());
    }

    public function show(Request $request) 
    {
        return new UserResource(auth()->user());
    }
    
    public function store(UserRequest $request) 
    {
        $data = $request->all();
        $user = User::create($data);
        $user->save();
        $user->assignRole($request->usertype);
        return new UserResource($user);
    }
    
    public function destroy(Request $request, User $user)
    {
        $user->delete();
        return response()->json([
            'message' => 'Success'
        ]);
    }
    
    public function update(Request $request, User $user)
    {
        $data = $request->all();
        $user->update($data);
        return response()->json([
            'message' => 'Success'
        ]);
    }
}