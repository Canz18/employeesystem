<?php

namespace App\Http\Middleware;

use Closure;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(intval($request->age) < 18) {
            return response()->json( [ 
                'message' => 'Age is not accepted'
            ]);
        }
        return $next($request);
    }
}
