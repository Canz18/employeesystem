<?php

namespace App\Http\Middleware;

use App\Http\Resources\UserResource;
use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->hasAnyRole(['super admin', 'admin']))
        {
            return $next($request);
        }
        return response()->json(['message' => 'Unauthorized.'], 401);
    }
}
