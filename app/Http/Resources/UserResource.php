<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'api_token' => $this->api_token,
            'birthdate' => $this->birthdate,
            'address' => $this->address,
            'gender' => $this->gender,
            'contact_number' => $this->contact_number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'role' => $this->roles->pluck('name'),
            'isAdmin' => $this->hasAnyRole('super admin', 'admin')
        ];
    }
}
