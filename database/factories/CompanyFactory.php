<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Company;
use App\User;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        'company_address' => $faker->address
    ];
});


$factory->define(User::class, function (Faker $faker) {
    $gender = $faker->randomElements(['male', 'female'])[0];
    return [
        'name' => $faker->name($gender),
        'email' => $faker->email,
        'password' => 'password123',
        'birthdate' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'address' => $faker->address,
        'gender' => $gender,
        'contact_number' => $faker->tollFreePhoneNumber
    ];
});
