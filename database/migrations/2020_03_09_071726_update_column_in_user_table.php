<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('birthdate', 10)->after('password')->nullable();
            $table->string('address', 255)->after('birthdate')->nullable();
            $table->string('gender',6)->after('address')->nullable();
            $table->string('contact_number', 20)->after('gender')->nullable();
            $table->string('system_position', 30)->after('contact_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('birthdate');
            $table->dropColumn('address');
            $table->dropColumn('gender');
            $table->dropColumn('contact_number');
            $table->dropColumn('system_position');
        });
    }
}
