<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Company::class, 5)->create()->each(function ($company) {
            $users = factory(App\User::class, 5)->make();
            $company->users()->saveMany($users);
        });
    }
}
