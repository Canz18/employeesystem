
const LoginReducer = (state = [], action) => {
    switch (action.type) {
      case 'USERS_LOGIN_REQUEST':
        return {
          loggingIn: true
        }
      case 'USERS_LOGIN_SUCCESS':
        return {
          user: action.payload.user
        }
      case 'USERS_LOGIN_FAILURE':
        return {
          error: action.payload.error
        }
      case 'USER_LOG_OUT':
        return {}
      default:
        return state
    }
  }

  export default LoginReducer