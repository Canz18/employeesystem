const RegistrationReducer = (state = [], action) => {
    switch (action.type) {
      case 'USER_REGISTRATION_REQUEST':
        return { registered: true }
      case 'USER_REGISTRATION_SUCCESS':
        return {
            message: action.payload.message
        }
      case 'USER_REGISTRATION_FAILURE':
        return {
            error: action.payload.error
        }
      default:
        return state
    }
  }

  export default RegistrationReducer