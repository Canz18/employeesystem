const RolePermissionReducer = (state = [], action) => {
    switch (action.type) {
      case 'ROLE_PERMISSION_REQUEST':
        return { registered: true }
      case 'ROLE_PERMISSION_SUCCESS':
        return {
            data: action.payload.data,
            modal: false
        }
      case 'ROLE_PERMISSION_FAILURE':
        return {
            error: action.payload.error,
            modal: false
        }
      default:
        return state
    }
  }

  export default RolePermissionReducer;