
const UserReducer = (state = [], action) => {
    switch (action.type) {
      case 'GET_USER_REQUEST':
        return {}
      case 'GET_USER_SUCCESS':
        return {
          user: action.payload.user
        }
      case 'GET_USER_FAILURE':
        return {
          showErrMgs: true,
          error: action.payload.error
        }
      default:
        return state
    }
  }

  export default UserReducer