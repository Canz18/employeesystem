import React from 'react'
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
export const DeleteModal = ({showModal, handleClose, handleDeleteRole}) => {
    return (
        <Modal show={showModal} onHide={handleClose} centered>
            <Modal.Header closeButton>
             <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>Are you sure, You want to delete?</div>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="default" onClick={handleDeleteRole}>
                Yes
            </Button>
            <Button variant="primary" onClick={handleClose}>
                No
            </Button>
            </Modal.Footer>
        </Modal>
    )
}