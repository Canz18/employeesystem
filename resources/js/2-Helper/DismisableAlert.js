import Alert from 'react-bootstrap/Alert';
import React from 'react';

export const DismisableAlert = (show, header, message) => {
    return (
        <Alert className="alertCls" variant="danger" onClose={() => show()} dismissible>
            <Alert.Heading>header</Alert.Heading>
            <p>message</p>
        </Alert>
    )
}