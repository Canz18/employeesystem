import { useEffect, useRef } from 'react';

export const GlobalFunction = {
    CheckCurrentAndPrevData
}

function CheckCurrentAndPrevData(value) {
    const prevProps = useRef();
    useEffect(() => {
        prevProps.current = value;
    })

    const preData = prevProps.current;
    return (preData !== value) ? true : false;
}