import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
export const IdleTimeOutModal = ({showModal, handleClose, handleLogout, remainingTime, userfullname}) => {
    const [timer, setTimer] = useState(remainingTime/1000);
   
    const runTimer = () => {
        var data;
        var i = (remainingTime/1000);  
        data = setInterval(() => {
            if(i==0){clearInterval(data); return;}
                setTimer(i);
                i = i - 1;
        },1000); 
    }

    return (
        <Modal show={showModal} onHide={handleClose} onShow={runTimer} centered>
            <Modal.Header closeButton>
             <Modal.Title> Session Expiring</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>Your secure connection to Page is about to time-out</div>
                <div>You will be automatically logged out in {timer} seconds</div>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="default" onClick={handleLogout}>
                Log Out Now
            </Button>
            <Button variant="primary" onClick={handleClose}>
                Stay Connected
            </Button>
            </Modal.Footer>
        </Modal>
    )
}