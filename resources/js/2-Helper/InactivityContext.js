import { createContext } from 'react';

export const InactivityContext = createContext(null);