import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
export const ModalPermission = ({showModal, handleClose, handleCreate}) => {
    const [permissionname, setPermissionname] = useState('');
    const input_data = (e) => {
        setPermissionname(e.target.value);
    }

    return (
        <Modal show={showModal} onHide={handleClose} centered>
            <Modal.Header closeButton>
             <Modal.Title> Role </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-group"><label className="small mb-1" >Permission Name</label>
                    <input className="form-control" id="permission" type="text" placeholder="Enter Permission name" 
                    values={permissionname} 
                    onChange={input_data} />
                </div>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="default" onClick={() => handleCreate(permissionname)}>
                Create
            </Button>
            <Button variant="primary" onClick={handleClose}>
                Cancel
            </Button>
            </Modal.Footer>
        </Modal>
    )
}