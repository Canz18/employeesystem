import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
export const ModalRole = ({showModal, handleClose, handleCreate}) => {
    const [rolename, setRolename] = useState('');
    const input_data = (e) => {
        setRolename(e.target.value);
    }

    return (
        <Modal show={showModal} onHide={handleClose} centered>
            <Modal.Header closeButton>
             <Modal.Title> Role </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-group"><label className="small mb-1" >Role Name</label>
                    <input className="form-control" id="rolename" type="text" placeholder="Enter Role name" 
                    values={rolename} 
                    onChange={input_data} />
                </div>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="default" onClick={() => handleCreate(rolename)}>
                Create
            </Button>
            <Button variant="primary" onClick={handleClose}>
                Cancel
            </Button>
            </Modal.Footer>
        </Modal>
    )
}