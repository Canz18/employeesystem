import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
export const PermissionInRoleModal = ({showModal, handleClose, handleAdd, handleSelect}) => {
    const [permissionname, setPermissionname] = useState('');
    const input_data = (e) => {
        setPermissionname(e.target.value);
    }

    return (
        <Modal show={showModal} onHide={handleClose} centered>
            <Modal.Header closeButton>
                <Modal.Title> Session Expiring</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-group"><label className="small mb-1" >Permission Name</label>
                    <select className="form-control" id="chooseData" onChange={input_data} >
                        <option value="">Select Permission</option>
                        {
                            handleSelect.data &&
                                handleSelect.data.map((e, i) => <option value={e.name} key={i}> {e.name} </option>)
                        }
                    </select>
                </div>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="default" onClick={() => handleAdd(permissionname)}>
                Add
            </Button>
            <Button variant="primary" onClick={handleClose}>
                Cancel
            </Button>
            </Modal.Footer>
        </Modal>
    )
}