import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import LayOut from '../components/LayOut'

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('localItem')
            ? (
                <LayOut { ...props }>
                    <Component {...props} />
                </LayOut>
            )
            : <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    )} />
);