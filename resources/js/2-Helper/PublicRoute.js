import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PublicRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        !localStorage.getItem('localItem')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/main/1', state: { from: props.location } }} />
    )} />
);