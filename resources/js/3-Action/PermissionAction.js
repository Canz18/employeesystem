import axios from 'axios';
export const PermissionAction = {
    retrievePermissionPerRole,
    getAllPermissionNotInRole,
    revokePermissionInRole,
    getAllPermission,
    createPermission,
    deletePermission
}

function deletePermission (dispatch, id) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/permissions/'+ id
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: {  data: "Permission deleted Successfully "  }});
    })
    .catch(error => { 
        let dataVal = error.response;
        console.log(dataVal)
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function createPermission (dispatch, name) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/permissions',
        data: { name },
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: {  data: "Permission Successfully added"  }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function getAllPermission (dispatch) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'GET',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/permissions',
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: { data: response.data }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function revokePermissionInRole (dispatch, objArr) {
    const { id, name } = objArr;
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})

    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/roles/permission/'+ id,
        data: { permissions: name },
    };
    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: {  data: "Successfully Delete"  }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function getAllPermissionNotInRole (dispatch, roleId) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'GET',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/permission/' + roleId + '/role',
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: { data: response.data }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function retrievePermissionPerRole (dispatch, id) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'GET',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/roles/permission/' + id,
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: { data: response.data }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

