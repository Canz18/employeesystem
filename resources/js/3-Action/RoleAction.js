import axios from 'axios';
export const RoleAction = {
    retrieveRole,
    createRole,
    deleteRole,
    addPermissionInRole,
}


function addPermissionInRole (dispatch, objArr) {
    const { id, name } = objArr;
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'PATCH',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/roles/'+ id,
        data: { permissions: name },
    };
    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: {  data: "Successfully Add Permission"  }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function retrieveRole (dispatch) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'GET',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/roles',
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: { data: response.data }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function deleteRole (dispatch, id) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/roles/'+ id
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: {  data: "Successfully Deleted Role"  }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}

function createRole (dispatch, rolename) {
    dispatch({type: 'ROLE_PERMISSION_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/roles',
        data: { rolename },
    };

    axios(requestOptions).then( response => {
        dispatch({type: 'ROLE_PERMISSION_SUCCESS', payload: {  data: "Successfully Registered"  }});
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.data.errors} });
        } if(dataVal.status == 401) {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        } else {
            dispatch({ type: 'ROLE_PERMISSION_FAILURE', payload: { error: dataVal.statusText} });
        }
    })
}