import axios from 'axios';
export const UserAction = {
    LoginAction,
    logOut,
    getCurrentUser,
    registerUser
}

async function LoginAction(username, password, dispatch, history) {
        dispatch({type: 'USERS_LOGIN_REQUEST'})
        const requestOptions = {
            method: 'POST',
            headers: { 'Accept': 'application/json','Content-Type': 'application/json' },
            url: '/api/login',
            data: { email: username, password }
        };
    
    await axios(requestOptions).then( response => {
            const localUser = {
                username,
                token: response.data.type + ' ' + response.data.token
            }
            const user = window.btoa(JSON.stringify(localUser));

            localStorage.setItem('localItem', JSON.stringify(user));
            
            dispatch({type: 'USERS_LOGIN_SUCCESS', payload: { user: username }});
            history.push("/main/1");
        })
        .catch(error => { 
            let dataVal = error.response;
            if(dataVal.status === 422 || dataVal.status === 429) {
                dispatch({ type: 'USERS_LOGIN_FAILURE', payload: { error: dataVal.data.errors.email[0]} });
            } else {
                if (localStorage.getItem('localItem')) { localStorage.removeItem('localItem')}
                dispatch({ type: 'USERS_LOGIN_FAILURE', payload: { error: dataVal.statusText} });
                history.push('/');
            }
        })
        
}
function logOut(history, dispatch) {
    dispatch({type: 'USER_LOG_OUT'});

    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'GET',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/logout',
    };

 axios(requestOptions).then( response => {
        dispatch({ type: 'USERS_LOGIN_SUCCESS', payload: { user: response.data.message} });
        localStorage.removeItem('localItem');
        history.push('/');
    });
    
}

function getCurrentUser(dispatch, history) {
    dispatch({type: 'GET_USER_REQUEST'})
    const localVal = JSON.parse(localStorage.getItem('localItem'));
    const obj = JSON.parse(window.atob(localVal));
    const requestOptions = {
        method: 'GET',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json','Authorization': obj.token },
        url: '/api/auth-user',
    };
    
    axios(requestOptions).then( response => {
            dispatch({ type: 'GET_USER_SUCCESS', payload: { user: response.data.data } });
        })
        .catch(error => { 
            let dataVal = error.response;
            if(dataVal.status === 422 || dataVal.status === 429) {
                dispatch({ type: 'GET_USER_FAILURE', payload: { error: dataVal.data.errors.email[0] } });
            } else {
                dispatch({ type: 'GET_USER_FAILURE', payload: { error: dataVal.statusText } });
                if (localStorage.getItem('localItem')) { localStorage.removeItem('localItem')}
                history.push('/');
            }
        })
}

async function registerUser(dispatch, obj) {
    dispatch({type: 'USER_REGISTRATION_REQUEST'});
    const requestOptions = {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json' },
        url: '/api/user_registration',
        data: {
                name : obj.inputFullname,
                email : obj.inputEmailAddress,
                password : obj.inputPassword,
                password_confirmation : obj.inputConfirmPassword,
                birthdate : obj.inputBirthdate,
                address : obj.inputAddress,
                gender : obj.inputGender,
                contact_number : obj.inputContactNo,
                usertype : obj.usertype
        }
    };

    await axios(requestOptions).then( response => {
         dispatch({ type: 'USER_REGISTRATION_SUCCESS', payload: { message: "Successfully Registered" } });
    })
    .catch(error => { 
        let dataVal = error.response;
        if(dataVal.status === 422 || dataVal.status === 429) {
            dispatch({ type: 'USER_REGISTRATION_FAILURE', payload: { error: dataVal.data.errors } });
        } else {
            dispatch({ type: 'USER_REGISTRATION_FAILURE', payload: { error: dataVal.statusText } });
        }
    })
}
