import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Switch, Route, Router } from 'react-router-dom';
import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk';

import { createLogger } from 'redux-logger'

import LoginComponent from './LoginComponent';
import MainComponent from './MainComponent';
import RegisterComponent from './RegisterComponent';
import ChartComponent from './ChartComponent';
import RolesPermissionComponents from './RolesPermissionComponents'
import RegisterEmpComponent from './RegisterEmpComponent';
import Error404Component from './Error404Component';
import Error401Component from './Error401Component';
import ForgotPasswordComponent from './ForgotPasswordComponent';

import LoginReducer from '../0-Reducer/LoginReducer';
import { PrivateRoute } from '../2-Helper/PrivateRoute';
import { PublicRoute } from '../2-Helper/PublicRoute';
import { InactivityContext } from '../2-Helper/InactivityContext';

const RootReducer = combineReducers({
    LoginReducer
});
const loggerMiddleware = createLogger();
const store = createStore(RootReducer, applyMiddleware(
    thunkMiddleware, loggerMiddleware
))

const history = createBrowserHistory();

export default class Example extends Component {

    render() {
        return (
            
            // <InactivityContext.Provider value=''>
                <Router  history={history}>
                    <Switch>
                        <PublicRoute exact path="/" component={LoginComponent} />
                        <PrivateRoute path="/main/:id" component={MainComponent} />
                        <PrivateRoute path="/chart" component={ChartComponent} />
                        <PrivateRoute path="/rolespermission" component={RolesPermissionComponents} />
                        <PrivateRoute path="/registerEmployee" component={RegisterEmpComponent} />
                        <Route path="/forgotpassword" component={ForgotPasswordComponent} />
                        <Route path="/error401" component={Error401Component} />
                        <Route path="/register" component={RegisterComponent} />
                        <Route path="" component={Error404Component} />
                    </Switch>
                </Router>
            // </InactivityContext.Provider>
        );
    }
}

ReactDOM.render(<Provider store = {store}>
    <Example />
</Provider>, document.getElementById('example'));