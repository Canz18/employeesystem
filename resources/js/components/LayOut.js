import React, { useState, useReducer, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { UserAction } from '../3-Action/UserAction';
import LoginReducer from '../0-Reducer/LoginReducer';
import UserReducer from '../0-Reducer/UserReducer';
import { IdleTimeOutModal } from '../2-Helper/IdleTimeOutModal';
import { Context } from '../2-Helper/Context';
// import { InactivityContext } from '../2-Helper/InactivityContext';


import IdleTimer from 'react-idle-timer';

const LayOut = props => {
    // const IdleMessage = useContext(Context);
    const [toogle, setToogle] = useState("On");
    const [reducerState, dispatch] = useReducer(LoginReducer, []);
    const [reducerUser, userdispatch] = useReducer(UserReducer, []);
    const [idleState, setIdleState] = useState({
        timeout: 5000 * 5,
        showModal: false,
        userLoggedIn: false,
        isTimedOut: false,
        sample: 0
    })
    var idleTimer =  null;
    

    useEffect(() => {
        UserAction.getCurrentUser(userdispatch, props.history);
    },[])

   
    const sidebarToggle = (e) => {
        e.preventDefault();
        
        if(toogle === "On") {
            var element = document.getElementsByTagName("body")[0];
            element.classList.add("sb-sidenav-toggled");
            setToogle("Off")
        } else {
            var element = document.getElementsByTagName("body")[0];
            element.classList.remove("sb-sidenav-toggled");
            setToogle("On")
        }
    }

    const onAction = (e) => {
        console.log('user did something')
        // setIdleState({...idleState, 
        //     isTimedOut: false})
      }
     
      const  onActive = (e) => {
        // console.log('user is active', e)
        setIdleState({...idleState, isTimedOut: false})
      }
     
      const onIdle = (e) => {
        
        const isTimedOut = idleState.isTimedOut;
        if (isTimedOut) {
            UserAction.logOut(props.history, dispatch);
        } else {
            idleTimer.reset();
            setIdleState({...idleState, showModal: true, isTimedOut: true})
        }
        // console.log('user is idle', idleState)
      }
    
      const handleClose = () => {
        idleTimer.reset();
        setIdleState({...idleState, showModal: false, isTimedOut: false});
        // console.log('user handleClose', idleState)
      }

    const handle_logout = (e) => {
        e.preventDefault();
        UserAction.logOut(props.history, dispatch);
    }


return (
       
        <div>
            <IdleTimer
                        ref={ref => { idleTimer = ref }}
                        element={document}
                        onActive={onActive}
                        onIdle={onIdle}
                        onAction={onAction}
                        debounce={250}
                        timeout={idleState.timeout} />

            <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
                <a className="navbar-brand" href="/">Sample System</a><button className="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#" onClick={sidebarToggle}><i className="fas fa-bars"></i></button>
                <form className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                    {/* <div className="input-group">
                        <input className="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                        <div className="input-group-append">
                            <button className="btn btn-primary" type="button"><i className="fas fa-search"></i></button>
                        </div>
                    </div> */}
                </form>
                <ul className="navbar-nav ml-auto ml-md-0">
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"  aria-expanded="false"><i className="fas fa-user fa-fw"></i></a>
                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <a className="dropdown-item" href="#">Settings</a><a className="dropdown-item" href="#">Activity Log</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#" onClick={handle_logout}>Logout</a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div className="sb-sidenav-menu">
                            <div className="nav">
                                <div className="sb-sidenav-menu-heading">Core</div>
                                <a className="nav-link" href="/">
                                    <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                    Dashboard
                                </a>
                                <div className="sb-sidenav-menu-heading"></div>
                                <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts_banks" aria-expanded="false" aria-controls="collapseLayouts_banks"
                                    ><div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                                    Banks
                                    <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </a>
                                <div className="collapse" id="collapseLayouts_banks" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <nav className="sb-sidenav-menu-nested nav">
                                        {/* {data &&
                                            data.map((e, i) => <Link className="nav-link" style={{fontSize: "1px !important"}} to={`/account/${e.id}`}>{e.name} {e.balance}</Link>)
                                        } */}
                                        <Link className="nav-link" style={{fontSize: "1px !important"}} to={`/main/1`}>Main 1</Link>
                                        <Link className="nav-link" style={{fontSize: "1px !important"}} to={`/main/2`}>Main 2</Link>
                                        <a className="nav-link" href="/employeelist">Employee List</a>
                                        <a className="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                                    </nav>
                                </div>
                                <div className="sb-sidenav-menu-heading">Interface</div>
                                <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
                                    ><div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                                    Employee
                                    <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </a>
                                <div className="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <nav className="sb-sidenav-menu-nested nav">
                                        <a className="nav-link" href="/employeelist">Employee List</a>
                                        <a className="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                                        <a className="nav-link" href="/registerEmployee">Register Employee</a>
                                    </nav>
                                </div>
                                <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages"
                                    ><div className="sb-nav-link-icon"><i className="fas fa-book-open"></i></div>
                                    Pages
                                    <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </a>
                                <div className="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                    <nav className="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                        <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth"
                                            >Authentication
                                            <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                        </a>
                                        <div className="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                            <nav className="sb-sidenav-menu-nested nav"><a className="nav-link" href="/login">Login</a><a className="nav-link" href="/register">Register</a><a className="nav-link" href="/forgotpassword">Forgot Password</a></nav>
                                        </div>
                                        <div className="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                            <nav className="sb-sidenav-menu-nested nav"><a className="nav-link" href="401.html">401 Page</a><a className="nav-link" href="404.html">404 Page</a><a className="nav-link" href="500.html">500 Page</a></nav>
                                        </div>
                                    </nav>
                                </div>
                                <div className="sb-sidenav-menu-heading">Addons</div>
                                <Link className="nav-link" to="/chart"><div className="sb-nav-link-icon"><i className="fas fa-chart-area"></i></div>
                                    Charts</Link>
                                <a className="nav-link" href="tables.html"><div className="sb-nav-link-icon"><i className="fas fa-table"></i></div>
                                    Tables</a>
                                {
                                    (reducerUser.user && reducerUser.user.isAdmin) &&
                                    <Link className="nav-link" to="/rolespermission"><div className="sb-nav-link-icon"><i className="fas fa-chart-area"></i></div>
                                    Roles & Permissions</Link> 
                                }
                            </div>
                        </div>
                        <div className="sb-sidenav-footer">
                            <div className="small">Logged in as:</div>
                           { reducerUser.user && 
                                <label>{reducerUser.user.name}</label>
                            }
                        </div>
                    </nav>
                </div>
                <div id="layoutSidenav_content">
                    {
                         reducerUser.user && 
                            <main>
                                <Context.Provider value={reducerUser.user}>
                                    {props.children }
                                </Context.Provider>
                            </main>
                    }
                    
                    <footer className="py-4 bg-light mt-auto">
                        <div className="container-fluid">
                            <div className="d-flex align-items-center justify-content-between small">
                                <div className="text-muted">Copyright &copy; Your Website 2019</div>
                                <div>
                                    <a href="#">Privacy Policy</a>
                                    &middot;
                                    <a href="#">Terms &amp; Conditions</a>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
            {
                reducerUser.user && 
                <IdleTimeOutModal 
                showModal={idleState.showModal} 
                handleClose={handleClose}
                handleLogout={handle_logout}
                remainingTime={idleState.timeout}
                userfullname={ reducerUser.user.name }
            />
            }
            
        </div>
    )
}

export default LayOut; 