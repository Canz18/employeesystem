import React, { useState, useReducer } from 'react';
import { Link } from 'react-router-dom';
import LoginReducer from '../0-Reducer/LoginReducer';
import { UserAction } from '../3-Action/UserAction';


const LoginComponent = props => {
        const [submit, setSubmit] = useState(false);
        const [state, setState] = useState({
            username: '',
            password: ''
        });
        const [reducerState, dispatch] = useReducer(LoginReducer, []);

        const handle_input = (e) => {
            setState({ ...state,
                [e.target.name]: e.target.value
            });
        }

        const handle_login = (e) => {
            e.preventDefault();
            setSubmit(true);
            if(state.password && state.username) {
                UserAction.LoginAction(state.username, state.password, dispatch, props.history)
            }
        }

        return(
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-lg-5">
                                    <div className="card shadow-lg border-0 rounded-lg mt-5">
                                        <div className="card-header"><h3 className="text-center font-weight-light my-4">Login</h3></div>
                                        <div className="card-body">
                                            <form>
                                                <div className="form-group">
                                                    <label className="small mb-1" >Email</label>
                                                    <input className={'form-control py-4' + (submit && !state.username ? ' hasError' : '')} name="username" id="inputEmailAddress" 
                                                    type="email" placeholder="Enter email address"  
                                                    onChange={ handle_input } 
                                                    value={ state.username } disabled={!reducerState.loggingIn || !submit ? false : true }/>
                                                    {submit && !state.username &&
                                                        <div className="textError">Username is required</div>
                                                    }
                                                </div>
                                                <div className="form-group">
                                                    <label className="small mb-1" >Password</label>
                                                    <input  className={'form-control py-4' + (submit && !state.password ? ' hasError' : '')} 
                                                        name="password" id="inputPassword" 
                                                        type="password" placeholder="Enter password"  
                                                        onChange={ handle_input } 
                                                        value={ state.password } disabled={!reducerState.loggingIn || !submit ? false : true }/>
                                                     {submit && !state.password &&
                                                        <div className="textError">Password is required</div>
                                                    }
                                                </div>
                                                <div className="form-group">
                                                    <div className="custom-control custom-checkbox">
                                                        <input className="custom-control-input" id="rememberPasswordCheck" 
                                                        type="checkbox"  disabled={!reducerState.loggingIn || !submit ? false : true }/>
                                                        <label className="custom-control-label" >Remember password</label>
                                                    </div>
                                                </div>
                                                <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                    <a className="small" href="password.html">Forgot Password?</a>
                                                    <a className={`btn btn-primary ${!reducerState.loggingIn || !submit ? "" : "isDisabled" }`} href="#" 
                                                    onClick={ handle_login }  disabled={!reducerState.loggingIn || !submit ? false : true }>Login</a>
                                                </div>
                                                <div style={{textAlign: "center", color: "Red", marginTop: "10px"}}>
                                                    { reducerState.error && <label>{reducerState.error}</label>}
                                                    { reducerState.loggingIn &&<img style={{width: "55px", height: "55px"}} src="https://media.giphy.com/media/sSgvbe1m3n93G/giphy.gif"/>}
                                                </div>
                                            </form>
                                        </div>
                                        <div className="card-footer text-center">
                                            <div className="small">
                                                <Link to="/register">Need an account? Sign up!</Link></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <style>
                    {
                        `
                            body {
                                background-color: #007bff !important;
                            }

                            .isDisabled {
                                color: currentColor;
                                cursor: not-allowed;
                                opacity: 0.5;
                            }

                            .hasError {
                                border-color: Red;
                            }

                            .textError {
                                color: Red;
                            }
                        `
                    }
                </style>
            </div>
        )
}

export default LoginComponent;