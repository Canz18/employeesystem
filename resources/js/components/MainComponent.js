import React, {useState, useEffect, useRef, useContext } from 'react';
import { Context } from '../2-Helper/Context';
import { useParams } from 'react-router';
import { GlobalFunction } from '../2-Helper/GlobalFunction';

const MainComponent = props => {
    const userRole = useContext(Context);
    const { id } = useParams();
    // const prevProps = useRef();
    const alertData = (propId) => {
        // alert(propId);
        // console.log(userRole.role[0]);
    }
 
    const viewComment = (e) => {
        e.preventDefault();
        // var x = document.getElementById("item1").nextSibling.innerHTML;
        //e.target.nextSibling.style.visibility = "visible"
        // var isVisible = (e.target.nextSibling.style.visibility)
        
        var isVisible = e.target.nextSibling.style.display === "none" ? "inline" : "none"; 
        var stext = isVisible === "none" ? "hide comment" : "view comment"; 
        e.target.nextSibling.style.display = isVisible;
        e.target.text = stext;
    }
    
    useEffect(() => {
        alertData(id)
    }, [id]); // act as componentdidmout and componentDidUpdate, it will trigger both after the load of component and if the id is changing

    useEffect(() => {
    },[]) // act as componentdidmount it trigger with the compinent is load

    return(
        <div className="container">
            {/* <p><label>Counter: { id }</label></p> */}
            <div className="media mb-4">
                <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                <div className="media-body">
                    <h5 className="mt-0">Commenter Name</h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                    &nbsp; <a className="pull-right" href="#" onClick={viewComment}>view comment</a>
                    <div className="commentCls" style={{display: "none"}}>
                        <div className="media mt-4">
                            <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                            <div className="media-body">
                                <h5 className="mt-0">Commenter Name</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div>

                        <div className="media mt-4">
                            <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                            <div className="media-body">
                                <h5 className="mt-0">Commenter Name</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="media mb-4">
                <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                <div className="media-body">
                    <h5 className="mt-0">Commenter Name</h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                    &nbsp; <a className="pull-right" href="#" onClick={viewComment}>view comment</a>
                    <div className="commentCls" style={{display: "none"}}>
                        <div className="media mt-4">
                            <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                            <div className="media-body">
                                <h5 className="mt-0">Commenter Name</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div>

                        <div className="media mt-4">
                            <img className="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="" />
                            <div className="media-body">
                                <h5 className="mt-0">Commenter Name</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default MainComponent;

