import React, { useState, useReducer } from 'react';
import { Link } from 'react-router-dom';
import RegistrationReducer from '../0-Reducer/RegistrationReducer';
import { UserAction } from '../3-Action/UserAction';


const RegisterComponent = props => {

    const [reducerState, dispatch] = useReducer(RegistrationReducer, []);
    const [submit, setSubmit] = useState(false);
    const [inputState, setInputState] = useState({
        inputFullname: '',
        inputAddress: '',
        inputEmailAddress: '',
        inputPassword: '',
        inputConfirmPassword: '',
        inputContactNo: '',
        inputGender: '',
        inputBirthdate: '',
        usertype: 'user'
    });

    const input_data = (e) => {
        setInputState({
            ...inputState,
            [e.target.id] : e.target.value
        });
    }

    const reg_submit = (e) => {
        e.preventDefault();
        setSubmit(true);
        UserAction.registerUser(dispatch, inputState);
    }

    const returnObjEmpty = (errorObj, param) => {
        if(errorObj) {
            return (errorObj[param]) ? true : false;
        }
        return false;
    }

    return(
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-lg-7">
                                    <div className="card shadow-lg border-0 rounded-lg mt-5">
                                        <div className="card-header"><h3 className="text-center font-weight-light">Create Account</h3></div>
                                        <div className="card-body">
                                            <form>
                                                <div className="form-row">
                                                    <div className="col-md-6">
                                                        <div className="form-group"><label className="small mb-1" >Full Name</label>
                                                            <input className="form-control" id="inputFullname" type="text" placeholder="Enter full name" values={inputState.inputFullname} onChange={input_data} disabled={!reducerState.registered || !submit ? false : true }/>
                                                            {!reducerState.registered && returnObjEmpty(reducerState.error, 'name') &&
                                                                <div className="help-block">{reducerState.error.name[0]}</div>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="form-group"><label className="small mb-1" >Contact Number</label>
                                                            <input className="form-control" id="inputContactNo" type="text" placeholder="Contact Number"  values={inputState.inputContactNo} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="form-group"><label className="small mb-1" >Birthdate</label>
                                                            <input className="form-control" id="inputBirthdate" type="text" placeholder=""  values={inputState.inputBirthdate} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-row">
                                                    <div className="col-md-4">
                                                        <div className="form-group"><label className="small mb-1" >Gender</label>
                                                            <select className="form-control" id="inputGender" onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }>
                                                                <option value="">select gender</option>
                                                                <option value="Female">Female</option>
                                                                <option value="Male">Male</option>
                                                            </select>
                                                            {/* <input className="form-control" id="inputGender" type="text" placeholder="Gender" values={inputState.inputGender} onChange={input_data}/> */}
                                                        </div>
                                                    </div>
                                                    <div className="col-md-8">
                                                        <div className="form-group"><label className="small mb-1" >Email</label>
                                                            <input className="form-control" id="inputEmailAddress" type="email" placeholder="Email Address"  values={inputState.inputEmailAddress} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                        {!reducerState.registered && returnObjEmpty(reducerState.error, 'email') &&
                                                                <div className="help-block">{reducerState.error.email[0]}</div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group"><label className="small mb-1" >Current Address</label><input className="form-control" id="inputAddress" type="text" placeholder="Address"  values={inputState.inputAddress} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/></div>
                                                <div className="form-row">
                                                    <div className="col-md-6">
                                                        <div className="form-group"><label className="small mb-1" >Password</label>
                                                            <input className="form-control" id="inputPassword" type="password" placeholder="Enter password"  values={inputState.inputPassword} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                        {!reducerState.registered && returnObjEmpty(reducerState.error, 'password') &&
                                                                <div className="help-block">{reducerState.error.password[0]}</div>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group"><label className="small mb-1" >Confirm Password</label><input className="form-control" id="inputConfirmPassword" type="password" placeholder="Confirm password"  values={inputState.inputConfirmPassword} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/></div>
                                                    </div>
                                                </div>
                                                {
                                                    reducerState.message && submit &&  
                                                    <div className="alert alert-danger" role="alert">
                                                        {reducerState.message}
                                                    </div>
                                                }
                                                <div className="form-group mt-4 mb-0"><a className="btn btn-primary btn-block" href="#" onClick={reg_submit}>Create Account</a></div>
                                            </form>
                                        </div>
                                        <div className="card-footer text-center">
                                            <div className="small"><Link to="/">Have an account? Go to login</Link></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <style>
                    {
                       `
                        body {
                            background-color: #007bff !important;
                        }

                        .help-block {
                            color: red;
                            font-size: 10px;
                        }
                       ` 
                    }
                </style>
            </div>
    )
}

export default RegisterComponent;