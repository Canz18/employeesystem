import React, { useState, useReducer, useEffect } from 'react';
import RegistrationReducer from '../0-Reducer/RegistrationReducer';
import { UserAction } from '../3-Action/UserAction';
import RolePermissionReducer from '../0-Reducer/RolePermissionReducer';
import { RoleAction } from '../3-Action/RoleAction';


const RegisterEmpComponent = props => {
    const [bulkAdd, setBulkAdd] = useState(false);
    const [reducerState, dispatch] = useReducer(RegistrationReducer, []);
    const [submit, setSubmit] = useState(false);
    const [inputState, setInputState] = useState({
        inputFullname: '',
        inputAddress: '',
        inputEmailAddress: '',
        inputPassword: '',
        inputConfirmPassword: '',
        inputContactNo: '',
        inputGender: '',
        inputBirthdate: '',
        usertype: ''
    });

    const [getRole, getRoleDispatch] = useReducer(RolePermissionReducer, []);

    useEffect(() => {
        RoleAction.retrieveRole(getRoleDispatch);
    }, [])

    const input_data = (e) => {
        setInputState({
            ...inputState,
            [e.target.id] : e.target.value
        });
    }

    const reg_submit = (e) => {
        e.preventDefault();
        setSubmit(true);
        UserAction.registerUser(dispatch, inputState);
    }

    const returnObjEmpty = (errorObj, param) => {
        if(errorObj) {
            return (errorObj[param]) ? true : false;
        }
        return false;
    }
    return(
        <main>
            <div className="container-fluid">
            
                <h1 className="mt-4">Register Employee</h1>
                <ol className="breadcrumb mb-4">
                    <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li className="breadcrumb-item active">Employee</li>
                </ol>
                <div className="card mb-8">
                    <div className="card-header"><i className="fas fa-chart-area mr-1"></i>User Register Form
                        {    bulkAdd == false ?
                    <               button className="btn btn-sm btn-outline-primary float-right" onClick={() => setBulkAdd(true)}>Upload Data</button>
                                :   <button className="btn btn-sm btn-outline-primary float-right" onClick={() => setBulkAdd(false)}>Register</button>
                        }
                    </div>
                    <div className="card-body">
                        {
                            bulkAdd == false ? 
                            <div className="row" style={{marginTop: "10px"}}>
                                <div className="col-md-8 col-sm-8" style={{margin: "auto"}}>
                                    <form>
                                        <div className="form-row">
                                            <div className="col-md-6">
                                                <div className="form-group"><label className="small mb-1" >Full Name</label>
                                                    <input className="form-control" id="inputFullname" type="text" placeholder="Enter full name" values={inputState.inputFullname} onChange={input_data} disabled={!reducerState.registered || !submit ? false : true }/>
                                                    {!reducerState.registered && returnObjEmpty(reducerState.error, 'name') &&
                                                        <div className="help-block">{reducerState.error.name[0]}</div>
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-md-3">
                                                <div className="form-group"><label className="small mb-1" >Contact Number</label>
                                                    <input className="form-control" id="inputContactNo" type="text" placeholder="Contact Number"  values={inputState.inputContactNo} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                </div>
                                            </div>
                                            <div className="col-md-3">
                                                <div className="form-group"><label className="small mb-1" >Birthdate</label>
                                                    <input className="form-control" id="inputBirthdate" type="text" placeholder=""  values={inputState.inputBirthdate} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col-md-4">
                                                <div className="form-group"><label className="small mb-1" >Gender</label>
                                                    <select className="form-control" id="inputGender" onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }>
                                                        <option value="">select gender</option>
                                                        <option value="Female">Female</option>
                                                        <option value="Male">Male</option>
                                                    </select>
                                                    {/* <input className="form-control" id="inputGender" type="text" placeholder="Gender" values={inputState.inputGender} onChange={input_data}/> */}
                                                </div>
                                            </div>
                                            <div className="col-md-8">
                                                <div className="form-group"><label className="small mb-1" >Email</label>
                                                    <input className="form-control" id="inputEmailAddress" type="email" placeholder="Email Address"  values={inputState.inputEmailAddress} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                {!reducerState.registered && returnObjEmpty(reducerState.error, 'email') &&
                                                        <div className="help-block">{reducerState.error.email[0]}</div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-9">
                                                <div className="form-group"><label className="small mb-1" >Current Address</label><input className="form-control" id="inputAddress" type="text" placeholder="Address"  values={inputState.inputAddress} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/></div>
                                            </div>
                                            <div className="col-md-3">
                                                <div className="form-group"><label className="small mb-1" >User Type</label>
                                                    <select className="form-control" id="usertype" onChange={input_data}  disabled={!reducerState.usertype || !submit ? false : true }>
                                                        <option value="">select type</option>
                                                        {
                                                            getRole.data && getRole.data.map((e, i) => 
                                                                <option key={i} value={e.name}>{e.name}</option>
                                                            )
                                                        }
                                                    </select>
                                                    {/* <input className="form-control" id="inputGender" type="text" placeholder="Gender" values={inputState.inputGender} onChange={input_data}/> */}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col-md-6">
                                                <div className="form-group"><label className="small mb-1" >Password</label>
                                                    <input className="form-control" id="inputPassword" type="password" placeholder="Enter password"  values={inputState.inputPassword} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/>
                                                {!reducerState.registered && returnObjEmpty(reducerState.error, 'password') &&
                                                        <div className="help-block">{reducerState.error.password[0]}</div>
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="form-group"><label className="small mb-1" >Confirm Password</label><input className="form-control" id="inputConfirmPassword" type="password" placeholder="Confirm password"  values={inputState.inputConfirmPassword} onChange={input_data}  disabled={!reducerState.registered || !submit ? false : true }/></div>
                                            </div>
                                        </div>
                                        {
                                            reducerState.message && submit &&  
                                            <div className="alert alert-danger" role="alert">
                                                {reducerState.message}
                                            </div>
                                        }
                                        <div className="form-group mt-4 mb-0"><a className="btn btn-primary float-right" href="#" onClick={reg_submit}>Create Account</a></div>
                                    </form>
                                </div>
                            </div>
                                              : <div>Upload Data</div>
                        }
                    </div>
                    <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                </div>
            </div>
            <style>
                {
                    `
                        .imagecls {
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            width: 55px;
                            height: 55px;
                        } 
                    `
                }
            </style>
        </main>
    )
}

export default RegisterEmpComponent;