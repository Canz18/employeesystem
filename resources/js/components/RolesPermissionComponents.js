import React, { useReducer, useState, useEffect, useContext  } from 'react';
import { RoleAction } from '../3-Action/RoleAction';
import { PermissionAction } from '../3-Action/PermissionAction';
import RolePermissionReducer from '../0-Reducer/RolePermissionReducer';
import { ModalRole } from '../2-Helper/ModalRole';
import { DeleteModal } from '../2-Helper/DeleteRoleModal';
import { PermissionInRoleModal } from '../2-Helper/PermissionInRoleModal';
import { ModalPermission } from '../2-Helper/ModalPermission'
import { DismisableAlert } from '../2-Helper/DismisableAlert';
import { Context } from '../2-Helper/Context';

const RolesPermissionComponents = props => {
    const userRole = useContext(Context);
    const [roleState, roleDispatch] = useReducer(RolePermissionReducer, []);
    const [roleCreate, roleCrateDispatch] = useReducer(RolePermissionReducer, [{ modal: false }]);
    const [permissionState, permissionDispatch] = useReducer(RolePermissionReducer, []);
    const [roleDelete, roleDeleteDispatch] = useReducer(RolePermissionReducer, [{ modal: false }]);
    const [permissionInRoleState, PermissionInRoleDispatch] = useReducer(RolePermissionReducer, [{ modal: false }]);
    const [addPermInRole, addPermInRoleDispatch] = useReducer(RolePermissionReducer, []);
    const [delPermInRole, delPermInRoleDispatch] = useReducer(RolePermissionReducer,[]);

    const [addPermission, addPermissionDispatch] = useReducer(RolePermissionReducer, []);
    const [allPermission, allPermissionDispatch] = useReducer(RolePermissionReducer,[]);
    const [deletePermission, deletePermissionDispatch] = useReducer(RolePermissionReducer, []);

    const [choosenRole, setChoosenRole] = useState({
        name: '',
        id: 0,
        submitted: false
    });

    const [showModalAddPermInRole, setShowModalAddPermInRole] = useState(false)

    const [showModalPerm, setShowModalPerm] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [showDelState, setShowDelState] = useState({
        showModalDel: false,
        id: 0,
        type: "",
        permissionId: 0,
        permissionName: ""
    });

    useEffect(() => {
        if(!userRole.isAdmin){
            props.history.push('/error401');
        }
    })

    useEffect(() => {
        if(userRole.isAdmin){
            RoleAction.retrieveRole(roleDispatch);
            setShowDelState({...showDelState, showModalDel: roleDelete.modal});
            setShowModal(roleCreate.modal);
        }
        return () => { 
            setShowModal(false);
            setShowDelState({
                showModalDel: false,
                id: 0 
            });

            setChoosenRole({
                name: '',
                id: 0,
                submitted: false
            })
        }
    },[roleCreate, roleDelete]);

    useEffect(() => {
        if(userRole.isAdmin && permissionState.data) {
            PermissionAction.getAllPermissionNotInRole(PermissionInRoleDispatch, choosenRole.id);
        }
    }, [permissionState])

    useEffect(() => {
        if(userRole.isAdmin && choosenRole.id != 0) {
            PermissionAction.retrievePermissionPerRole(permissionDispatch, choosenRole.id);
            setShowDelState({...showDelState, showModalDel: delPermInRole.modal});
        }
    }, [addPermInRole, delPermInRole, addPermission, deletePermission])

    useEffect(() => {
        if(userRole.isAdmin) {
            PermissionAction.getAllPermission(allPermissionDispatch);
        }
    }, [addPermission, deletePermission]);

    const rolePermission = (e, id, name) => {
        e.preventDefault();
        setChoosenRole({...choosenRole, name, id, submitted: true });
        PermissionAction.retrievePermissionPerRole(permissionDispatch, id);
    }

    const handleClose = () => {
        setShowModal(false)
        setShowDelState({...showDelState, showModalDel: false});
        setShowModalAddPermInRole(false);
        setShowModalPerm(false);
    }
    
    const handleShowModal = () => {
        setShowModal(true);
    }

    const showModalPermission = () => {
        setShowModalPerm(true);
    }

    const handleDelShowModal = (e, id, type, permissionName) => {
        e.preventDefault();
        setShowDelState({...showDelState, showModalDel: true, id, type, permissionName});
    }

    const handlePermInRoleShowModal = () => {
        setShowModalAddPermInRole(true)
    }

    const btn_createRole =  (rolename) => {
        RoleAction.createRole(roleCrateDispatch, rolename);
    };

    const btn_delete = () => {
        switch(showDelState.type) {
            case 'role' :
                RoleAction.deleteRole(roleDeleteDispatch, showDelState.id);   
                break;
            case 'permission' :
                var obj = { id: choosenRole.id, name: showDelState.permissionName}
                PermissionAction.revokePermissionInRole(delPermInRoleDispatch, obj);
                break;
            case 'permission-1' :
                PermissionAction.deletePermission(deletePermissionDispatch, showDelState.id);
                break;
            default:
                
        } 
        
    }

    const btn_addPermInRole = (permissionName) => {
        var obj = {
            id: choosenRole.id,
            name: permissionName
        }

        RoleAction.addPermissionInRole(addPermInRoleDispatch, obj);
        setShowModalAddPermInRole(false);
    }

    const btn_addPermission = (permissionname) => {
        PermissionAction.createPermission(addPermissionDispatch, permissionname);
        setShowModalPerm(false);
    }

    return (
       
        <main>
            <ModalRole 
                showModal={showModal} 
                handleClose={handleClose}
                handleCreate={btn_createRole}
            />

            <ModalPermission 
                showModal={showModalPerm} 
                handleClose={handleClose}
                handleCreate={btn_addPermission}
            />

            <DeleteModal
                showModal={showDelState.showModalDel} 
                handleClose={handleClose}
                handleDeleteRole={btn_delete}
            />
           
           <PermissionInRoleModal 
                showModal={showModalAddPermInRole} 
                handleClose={handleClose} 
                handleAdd={btn_addPermInRole}
                handleSelect={permissionInRoleState}
           />
            <div className="container-fluid">
            
                <h1 className="mt-4">Role and Permission</h1>
                <ol className="breadcrumb mb-4">
                    <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li className="breadcrumb-item active">Permission</li>
                </ol>
                <div className="card mb-12">
                    <div className="card-header"><i className="fas fa-chart-area mr-1"></i>User Roles and Permission</div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-lg-3">
                                <div className="card mb-6">
                                    <div className="card-header"><i className="fas fa-chart-area mr-1"></i>Role <button className="btn btn-sm btn-outline-primary float-right" onClick={handleShowModal}>Add Role</button></div>
                                    <div className="card-body">
                                        <div>
                                            {
                                                roleState.data ? 
                                                                <table className="table table-sm table-striped table-hover ">
                                                                    <tbody>
                                                                        {
                                                                            roleState.data.map((e,i) => 
                                                                                <tr  key={i}>
                                                                                    <td style={{textAlign: "center"}} onClick={(j) => rolePermission(j, e.id, e.name)}>{e.name}</td>
                                                                                    <td><a href="#" style={{color: "red"}} className="float-right" title="Delete" data-toggle="tooltip" onClick={(j) => handleDelShowModal(j, e.id, "role", "")}>
                                                                                            <i className="material-icons">&#xE872;</i></a>
                                                                                    </td>
                                                                                </tr>
                                                                            )
                                                                        }
                                                                    </tbody>
                                                                </table>
                                                    : <img  className="imagecls" src="https://media.giphy.com/media/sSgvbe1m3n93G/giphy.gif"/>
                                            }
                                        </div>
                                    </div>
                                    <div className="card-footer small text-muted"></div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="card mb-6">
                                        <div className="card-header">
                                            <i className="fas fa-chart-area mr-1"></i>{ choosenRole.name != '' &&  'Permission/s  under ' + choosenRole.name.replace(/^\w/, c => c.toUpperCase()) } 
                                            {choosenRole.name != '' && <button className="btn btn-sm btn-outline-primary float-right" onClick={handlePermInRoleShowModal}>Add</button> }</div>
                                    <div className="card-body">
                                        {
                                            permissionState.data && choosenRole.submitted ?
                                                <table className="table table-sm table-striped ">
                                                    {}
                                                    <tbody>
                                                        {
                                                            permissionState.data.map((e,i) => 
                                                                <tr  key={i}>
                                                                    <td style={{textAlign: "center"}}>{e.name}</td>
                                                                    <td><a href="#" style={{color: "#E34724"}} className="delete float-right" title="Delete" data-toggle="tooltip" onClick={(j) => handleDelShowModal(j, choosenRole.id, "permission", e.name)}><i className="material-icons">&#xE872;</i></a></td>
                                                                </tr>
                                                            )
                                                        }
                                                    </tbody>
                                                </table> 
                                            : <label style={{textAlign: "center"}}>Select Role First</label>
                                        }
                                    </div>
                                    <div className="card-footer small text-muted"></div>
                                </div>       
                            </div>
                            <div className="col-lg-5">
                                <div className="card mb-6">
                                        <div className="card-header"><i className="fas fa-chart-area mr-1"></i>All Permissions  <button className="btn btn-sm btn-outline-primary float-right" onClick={showModalPermission}>Add Permission</button></div>
                                    <div className="card-body">
                                        {
                                            allPermission.data ? 
                                                        <table className="table table-sm table-striped table-hover ">
                                                            <tbody>
                                                                {
                                                                    allPermission.data.map((e,i) => 
                                                                        <tr  key={i}>
                                                                            <td style={{textAlign: "center"}}>{e.name}</td>
                                                                            <td><a href="#" style={{color: "red"}} className="float-right" title="Delete" data-toggle="tooltip" onClick={(j) => handleDelShowModal(j, e.id, "permission-1", "")}>
                                                                                    <i className="material-icons">&#xE872;</i></a>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                }
                                                            </tbody>
                                                        </table>
                                            : <img  className="imagecls" src="https://media.giphy.com/media/sSgvbe1m3n93G/giphy.gif"/>
                                        }
                                    </div>
                                    <div className="card-footer small text-muted"></div>
                                </div>       
                            </div>
                        </div>
                    </div>
                    <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                </div>
            </div>
            {/* <DismisableAlert show={handleClose}/> */}
            <style>
                {
                    `
                        .imagecls {
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            width: 55px;
                            height: 55px;
                        } 
                        
                        .alertCls {
                            position: fixed;
                            top: 10%;
                            left: 68%;
                            width: 30%;
                        }
                    `
                }
            </style>
        </main>
    )
}

export default RolesPermissionComponents;