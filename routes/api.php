<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('sampleIndex', 'SampleController@index')->name('sample.index');
Route::post('login', 'Auth\LoginController@login')->name('user.login');
Route::post('user_registration', 'UserController@store')->name('user.store');

Route::middleware([App\Http\Middleware\GuardNameMiddleware::class])->get('test',function () {
  return 1;
});

Route::group(['middleware' => 'ageAllow:api'],function(){
    Route::post('sample', 'sampleAgeController@index')->name('sample.index');
});

Route::group(['middleware' => 'auth:api'],function(){
    Route::get('auth-user', 'UserController@show')->name('user.show');
    Route::get('logout','Auth\LoginController@logout')->name('user.logout');
    Route::get('companies/user/{company}', 'Company\UserController@index')->name('company.user.index');
    
});
    

Route::group(['middleware' => ['auth:api','role:api']],function(){
    Route::post('permissions', 'PermissionController@store')->name('permission.store');
    Route::get('permissions', 'PermissionController@index')->name('permission.index');
    Route::delete('permissions/{permission}', 'PermissionController@destroy')->name('permission.destroy');
    Route::get('permission/{role}/role', 'PermissionController@getPermissionNotInRole')->name('parmission.getPermissionNotInRole');

    
    Route::get('roles/permission/{role}', 'Role\PermissionController@index')->name('permission.index');
    Route::delete('roles/permission/{role}', 'Role\PermissionController@destroy')->name('role.permission.destroy');

    Route::get('roles', 'RoleController@index')->name('role.index');
    Route::post('roles', 'RoleController@store')->name('role.store');
    Route::patch('roles/{role}', 'RoleController@update')->name('role.update');
    Route::delete('roles/{role}', 'RoleController@destroy')->name('role.destroy');

    
    Route::get('users/{user}/roles', 'User\RoleController@index')->name('user.role.index');
    Route::post('users/{user}/roles', 'User\RoleController@store')->name('user.role.store');
    Route::patch('users/{user}/roles', 'User\RoleController@update')->name('user.role.update');
    
    Route::get('user','UserController@index')->name('users.index');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
});

